(function () {
  var MODULE_NAME = 'shoppingcartModel',
    NETCENTRIC_NAMESPACE = 'nn';

  window[NETCENTRIC_NAMESPACE] = window[NETCENTRIC_NAMESPACE] || {};

  window[NETCENTRIC_NAMESPACE][MODULE_NAME] = function () {

    this._cart = {};
    this._vatRate = 0;

    function _getCart() {
      return this._cart;
    }

    return {
      init: function (vatRate) {
        this._cart = {
          total: {
            beforeVAT: 0,
            afterVAT: 0,
            VAT: 0
          },
          products: []
        };

        this._vatRate = vatRate || 0;
      },

      getCart: _getCart,

      addProducts: function (newOrExistingProducts) {

        let myProducts = [];
        // now addProducts allow Array and Object parameter
        if (Array.isArray(newOrExistingProducts)) {
          myProducts = newOrExistingProducts;
        } else {
          myProducts.push(newOrExistingProducts)
        }

        let tempProducts = {};
        myProducts.forEach(p => {
          // create temporary object with unique product
          if (tempProducts[p.name]) {
            // if the property already exist, we sum-up the quantity
            tempProducts[p.name] = {
              name: p.name,
              price: p.price,
              quantity: tempProducts[p.name].quantity + p.quantity
            }
          } else {
            tempProducts[p.name] = p;
          }
        });

        // each property represent a unique product with calculated quantity
        for (var propt in tempProducts) {
          var foundIndex = this._cart.products.findIndex(p => p.name === propt);
          if(foundIndex >= 0){
            this._cart.products[foundIndex].quantity =
              this._cart.products[foundIndex].quantity + tempProducts[propt].quantity;
          }else{
            this._cart.products.push(tempProducts[propt]);
          }
        }

        this.updateTotal();
        return this._cart;
      },

      updateTotal: function() {
        // transform and sum each items of the cart product
        if(this._cart.hasOwnProperty('total')){
          this._cart.total.beforeVAT = this._cart.products
            .map(p => Number(p.price) * Number(p.quantity))
            .reduce((a, b) => Number(a) + Number(b), 0);

          // avoid 0 based division, 2 decimals
          this._cart.total.VAT = this._vatRate === 0 ? 0 :
            Number((this._cart.total.beforeVAT * (this._vatRate / 100)).toFixed(2));

          this._cart.total.afterVAT =
            Number(this._cart.total.beforeVAT) + Number(this._cart.total.VAT);
        }
      },

      getProduct: function(name) {
        // ideally would be better to look for an ID
        let foundProduct = this._cart.products.find( e => e.name === name);
        return foundProduct ? foundProduct : null;
      },

      getProductQuantity: function (name) {
        // ideally would be better to look for an ID
        let currentProduct = this.getProduct(name);
        return currentProduct ? currentProduct.quantity : 0;
      },

      changeProductQuantity: function (name, newQuantity) {
        let foundProduct = this.getProduct(name);
        if(newQuantity <= 0) {
          this.removeProducts(name);
        } else {
          foundProduct.quantity = newQuantity;
        }
        this.updateTotal();
      },

      removeProducts: function (name) {
        this._cart.products = this._cart.products.filter(p => p.name !== name);
        if(this._cart.products.length === 0){
          this.destroy();
        }else {
          this.updateTotal();
        }
      },

      destroy: function () {
        this._cart.products.length = 0;
        this._cart.total.beforeVAT = 0;
        this._cart.total.afterVAT = 0;
        this._cart.total.VAT = 0;
      },
    };
  };
})();
