var shoppingCart = (function () {

  var instance = new window.nn.shoppingcartModel;
  var cart = {};

  /** initialize method. */
  function init(vat) {
    instance.init(vat);
    cart = instance.getCart();
    _drawCart();
    _bindAddToCartBtn();
    _bindCheckoutBtn();
  }

  /** return current cart */
  function getCart() {
    return cart;
  }

  /**
   * Add a product to the cart
   * @param {object} product - The current product to add.
   */
  function addProduct(product) {
    instance.addProducts(product);
    _drawCart();
  }

  /** return if the cart is empty */
  function isEmpty() {
    return !cart.hasOwnProperty('products') || cart.products.length <= 0;
  }

  /** Add a new product when the button from the catalogue is clicked  */
  function _bindAddToCartBtn() {
    var DOMAddToCartBtn = document.getElementsByClassName('add-to-cart');
    for (let item of DOMAddToCartBtn) {
      item.addEventListener('click', (event) => {
        var newProduct = JSON.parse(event.target.attributes['data-shop-listing'].nodeValue);
      newProduct['quantity'] = 1;
      addProduct(newProduct);
    });
    }
  }

  /** Increment and Decrement the quantity when - and + are clicked */
  function _bindIncreaseDecreaseBtn() {
    var DOMIncreaseBtn = document.getElementsByClassName('increase__quantity');
    var DOMDecreaseBtn = document.getElementsByClassName('decrease__quantity');
    for (let item of DOMIncreaseBtn) {
      item.addEventListener('click', (event) => {
        var product = JSON.parse(event.target.attributes['cart-item-data'].nodeValue);
        instance.changeProductQuantity(product.name, product.quantity + 1);
        _drawCart();
    });
    }
    for (let item of DOMDecreaseBtn) {
      item.addEventListener('click', (event) => {
        var product = JSON.parse(event.target.attributes['cart-item-data'].nodeValue);
        instance.changeProductQuantity(product.name, product.quantity - 1);
        _drawCart();
    });
    }
  }

  /**
   * Update the total when entering new quantity in input
   * if we enter 0 it removes the product from the cart
   * */
  function _bindInputQty() {
    var DOMInputQty = document.getElementsByClassName('input__quantity');
    for (let item of DOMInputQty) {
      item.addEventListener('keyup', (event) => {
        var product = JSON.parse(event.target.attributes['cart-item-data'].nodeValue);
        if(event.target.value === ''){
          instance.changeProductQuantity(product.name, 0);
          _drawCart();
        }else{
          instance.changeProductQuantity(product.name, Number(event.target.value));
          _drawCart();
        }
      });
    }
  }

  /** Remove the product from the cart when remove btn is clicked */
  function _bindRemoveBtn() {
    var DOMRemoveBtn = document.getElementsByClassName('remove__product');
    for (let item of DOMRemoveBtn) {
      item.addEventListener('click', (event) => {
        var product = JSON.parse(event.target.attributes['cart-item-data'].nodeValue);
        instance.removeProducts(product.name);
        _drawCart();
      });
    }
  }

  /** Revaluate and repaint the Total section in the cart */
  function _refreshTotal() {
    var DOMPriceBeforeVat = document.getElementsByName('price-before')[0];
    var DOMPriceVat = document.getElementsByName('vat-amount')[0];
    var DOMPriceAfterVat = document.getElementsByName('total-net')[0];

    DOMPriceBeforeVat.value = '\u20AC '+ parseFloat(cart.total.beforeVAT).toFixed(2);
    DOMPriceVat.value = '\u20AC '+ parseFloat(cart.total.VAT).toFixed(2);
    DOMPriceAfterVat.value = '\u20AC '+ parseFloat(cart.total.afterVAT).toFixed(2);
  }

  /** Change and make the button checkout disabled. we could possibly make the cart non-editable */
  function _bindCheckoutBtn() {
    var DOMCheckout = document.getElementsByClassName('proceed-checkout')[0];
    var DOMSending = document.getElementsByClassName('proceed-sending')[0];
    var DOMCart = document.getElementsByClassName('cart')[0];

    DOMCheckout.addEventListener('click',(event) => {
      event.preventDefault();
      DOMCheckout.classList.add('is-hidden');
      DOMSending.classList.remove('is-hidden');
      DOMCart.classList.add('cart-sending');
    });
  }

  /** Repaint each of the products in the Cart */
  function _refreshListProduct() {
    var DOMProductTable = document.getElementsByClassName('cart__items')[0];
    var DOMProductTemplate = document.getElementsByClassName('cart__item__template')[0];

    _flushProductTable();
    cart.products.forEach(product => {
      var row = DOMProductTable.insertRow();
    row.classList.add('cart__item');
    row.innerHTML = DOMProductTemplate.innerHTML;
    row.getElementsByClassName('cart__item__name')[0].innerHTML = product.name;
    row.getElementsByClassName('cart__item__label')[0].innerHTML = product.description;
    row.getElementsByClassName('cart__item__price')[0].childNodes[1].innerHTML =
      '\u20AC ' + product.price;
    row.getElementsByClassName('cart__item__quantity')[0].childNodes[1].value = product.quantity;

    function setAttributeData(element){
      element.setAttribute('cart-item-data', JSON.stringify({
        name: product.name,
        quantity: product.quantity
      }));
    }

    setAttributeData(row.getElementsByClassName('input__quantity')[0]);
    setAttributeData(row.getElementsByClassName('decrease__quantity')[0]);
    setAttributeData(row.getElementsByClassName('increase__quantity')[0]);
    setAttributeData(row.getElementsByClassName('remove__product')[0]);
  })

  }

  /**
   * Empty the cart by removing all product from the view,
   * Element .cart__item__template is used as a template to generate the new element.
   * It avoids having to generate too much HTML within Javascript
   * */
  function _flushProductTable() {
    // keeping cart__item__template
    var DOMCartItems = document.querySelectorAll('.cart__item:not(.cart__item__template)');
    for (let cartItem of DOMCartItems) {
      cartItem.parentNode.removeChild(cartItem);
    }
  }

  /** Repaint the Cart, the Total section and Do All Bindings */
  function _drawCart() {
    var DOMCartElement = document.getElementsByClassName('cart')[0];
    if (isEmpty()) {
      DOMCartElement.classList.add('is-hidden');
    } else {
      DOMCartElement.classList.remove('is-hidden');
      _refreshListProduct();
      _bindIncreaseDecreaseBtn();
      _bindInputQty();
      _bindRemoveBtn();
      _refreshTotal();
    }
  }

  return {
    init: init,
    getCart: getCart,
    isEmpty: isEmpty,
    addProduct: addProduct
  }
})();

const VAT = 20;
shoppingCart.init(VAT);
